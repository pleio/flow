from django.urls import re_path

from . import views

urlpatterns = [
    re_path(r"^login/$", views.login, name="login"),
    re_path(r"^login_response/$", views.login_response, name="login_response"),
    re_path(r"^request_access/$", views.request_access, name="request_access"),
    re_path(r"^retry/$", views.retry, name="retry"),
    re_path(r"^logout/$", views.logout, name="logout"),
]
