#!/usr/bin/env bash

echo "[i] Starting celery..."
celery -A flow.celery worker --queues=celery -B -E -O fair -s "/tmp/celerybeat-schedule" --loglevel=info
