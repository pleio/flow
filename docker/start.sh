#!/bin/bash

# Collect static
python manage.py collectstatic --noinput

# Run migrations
python manage.py migrate

# Start Gunicorn processes
echo Starting uwsgi
uwsgi --master --processes 4 --threads 2 --http :8000 --module flow.wsgi --static-map /static=/app/static
