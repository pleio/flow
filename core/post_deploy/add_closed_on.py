from post_deploy import post_deploy_action
from core.models import Case

@post_deploy_action
def task():
    for case in Case.objects.all():
        if case.status == "CLOSED":
            case.closed_on = case.logs.filter(event="changed_phase").order_by("created_on").last().created_on
            case.save()
