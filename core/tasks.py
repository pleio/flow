import csv
import os
import statistics
from email.utils import formataddr
from io import StringIO

from math import ceil
from celery import chord, shared_task, signature, exceptions
from celery.utils.log import get_task_logger
from django.conf import settings
from django.core.files import File
from django.core.files.storage import default_storage
from django.core.mail import send_mail
from django.template.loader import render_to_string
from django.utils import timezone
from django.utils.translation import gettext

from core.lib import Distribute, get_users_list_for_export, get_cases_list_for_export
from core.models import Case, CaseTypePhase, CsvExport, Attachment, ScanIncident
from core.utils import clamav
from pleio_auth.models import User

logger = get_task_logger(__name__)

from datetime import timedelta


def calculate_work_hours(
    start_date,
    end_date,
    workday_start=9,
    workday_end=17,
):
    def daily_work_hours(day):
        start_work_time = day.replace(
            hour=workday_start, minute=0, second=0, microsecond=0
        )
        end_work_time = day.replace(hour=workday_end, minute=0, second=0, microsecond=0)

        actual_start = max(start_date, start_work_time)
        actual_end = min(end_date, end_work_time)

        if actual_start >= actual_end:
            return 0

        return (actual_end - actual_start).total_seconds() / 3600

    total_work_hours = 0
    current_day = start_date

    while current_day.date() <= end_date.date():
        if current_day.weekday() < 5:  # Weekdagen (ma-vr)
            total_work_hours += daily_work_hours(current_day)
        current_day += timedelta(days=1)

    return total_work_hours


def is_max_duration_exceeded(case):
    if not case.latest_phase_change:
        return False
    if (
        calculate_work_hours(case.latest_phase_change, timezone.now())
        > case.current_phase.max_duration_hours
    ):
        return True
    return False


@shared_task
def sent_max_duration_exceeded_email(case_id):
    case = Case.objects.get(id=case_id)
    case.max_duration_case_phase_notification_sent = True
    case.save()
    for subscription in case.current_phase.subscription_set.all():
        try:
            user = subscription.user
            parameters = {
                "user": user,
                "case": case,
                "url": case.url,
            }

            send_mail(
                render_to_string(
                    "emails/phase_max_duration_exceeded_subject.txt", parameters
                ),
                render_to_string("emails/phase_max_duration_exceeded.txt", parameters),
                settings.EMAIL_FROM,
                [formataddr((user.name, user.email))],
                fail_silently=True,
            )
        except Exception as e:
            logger.error(e)


@shared_task()
def send_max_duration_case_phase_exceeded_emails():
    phases = CaseTypePhase.objects.filter(max_duration_hours__isnull=False)
    for phase in phases:
        for case in phase.case_set.filter(
            max_duration_case_phase_notification_sent=False
        ):
            if is_max_duration_exceeded(case):
                sent_max_duration_exceeded_email.delay(case.id)


@shared_task
def dispatch_hourly_cron():
    logger.info("Schedule hourly cron.")
    send_max_duration_case_phase_exceeded_emails.delay()


@shared_task
def create_users_export(user_ids, user_id):
    users = get_users_list_for_export(User.objects.filter(id__in=user_ids))
    user = User.objects.get(id=user_id)
    timestamp = timezone.now().strftime("%Y-%m-%d_%H-%M-%S")
    filename = f"users_{timestamp}.csv"

    csv_data = StringIO()
    csv_writer = csv.writer(csv_data, delimiter=";")
    for row in users:
        csv_writer.writerow(row)

    csv_file_obj = CsvExport()
    csv_file_obj.created_by = user
    csv_file_obj.csv_file.save(filename, File(csv_data))

    csv_file_obj.save()


@shared_task
def create_cases_export(case_ids, user_id):
    cases = get_cases_list_for_export(Case.objects.filter(id__in=case_ids))
    user = User.objects.get(id=user_id)
    timestamp = timezone.now().strftime("%Y-%m-%d_%H-%M-%S")
    filename = f"cases_{timestamp}.csv"

    csv_data = StringIO()
    csv_writer = csv.writer(csv_data, delimiter=";")
    for row in cases:
        csv_writer.writerow(row)

    csv_file_obj = CsvExport()
    csv_file_obj.created_by = user
    csv_file_obj.csv_file.save(filename, File(csv_data))

    csv_file_obj.save()


@shared_task
def schedule_scan_finished():
    incidents = ScanIncident.objects.filter(
        file_created__gte=timezone.now() - timezone.timedelta(days=1)
    )

    logger.info("Scanned found %i incidents", incidents.count())


@shared_task
def schedule_scan_attachments():
    scheduler = FileScanScheduler(distance=50)
    scheduler.schedule_scan()


class FileScanScheduler:
    def __init__(self, distance):
        self.distance = distance
        self.instances = []
        self.signatures = []

    def schedule_scan(self):
        self._collect_files()
        self._shuffle_files()
        self._create_signatures()
        self._schedule_signatures()

    def _collect_files(self):
        self.instances = []

        all_files = Attachment.objects.all()
        limit = ceil(all_files.count() / int(settings.SCAN_CYCLE_DAYS))
        for pk, size in all_files.values_list("pk", "size")[:limit]:
            self.instances.append(
                {
                    "file_guid": str(pk),
                    "file_size": size,
                }
            )

    def _shuffle_files(self):
        self.instances = [*sorted(self.instances, key=lambda x: x["file_size"])]
        self.instances = [*Distribute(self.instances, distance=self.distance).spread()]

    def _create_signatures(self):
        if len(self.instances) == 0:
            return
        start = timezone.now() + timedelta(minutes=1)
        mean = statistics.mean([i["file_size"] for i in self.instances])
        if len(self.instances) > 1:
            deviation = statistics.stdev([i["file_size"] for i in self.instances])
        else:
            deviation = 0
        for task in self.instances:
            start = start + timedelta(seconds=1)
            self.signatures.append(
                signature(
                    scan_file, args=(task["file_guid"],), eta=start
                )
            )

            # Give bigger files more time.
            if task["file_size"] > (mean + deviation):
                start = start + timedelta(seconds=5)

    def _schedule_signatures(self):
        close_task = schedule_scan_finished.si()
        chord(self.signatures, close_task).apply_async()


@shared_task(bind=True, rate_limit="30/m", max_retries=5, ignore_result=True)
def scan_file(self, file_id):
    try:
        file = Attachment.objects.filter(file=file_id).first()
        if not file:
            return

        if not file.file.name or not default_storage.exists(file.file.name):
            file.blocked = True
            file.block_reason = gettext("File not found on file storage")
            file.save()
            return

        logger.info(
            "Scan file %s, last scanned %s",
            os.path.basename(file.file.name),
            file.last_scan.isoformat()
        )
        file.last_scan = timezone.now()

        try:
            clamav.scan(file.file.name)
            file.blocked = False
            file.block_reason = None
            file.save()
            return True
        except clamav.FileScanError as e:
            ScanIncident.objects.create_from_attachment(e, file)
            if not e.is_virus():
                self.retry(eta=timezone.now() + timedelta(minutes=15))
            else:
                file.blocked = True
                file.block_reason = gettext("This file contains a virus")
                file.save()
            return False

    except exceptions.Retry:
        # Allow retry
        raise

    except Exception as e:
        # Make sure the task exits OK.
        return str(e)
