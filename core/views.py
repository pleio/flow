import logging
import os
from email.utils import formataddr
from itertools import chain
from urllib.parse import urljoin

import requests
from core.shared import scan_file
from django.conf import settings
from django.contrib import messages
from django.contrib.auth import authenticate
from django.contrib.auth import login as auth_login
from django.contrib.auth import logout as auth_logout
from django.contrib.auth.decorators import login_required
from django.contrib.postgres.search import SearchQuery, SearchRank, SearchVector
from django.core.mail import send_mail
from django.core.paginator import EmptyPage, PageNotAnInteger, Paginator
from django.db.models import F
from django.http import FileResponse, Http404, HttpResponseForbidden
from django.shortcuts import get_object_or_404, redirect, render
from django.template.loader import render_to_string
from django.urls import reverse

from core.lib import close_files, get_opened_files
from .validators import validate_file_type

from .forms import (
    CaseForm,
    CommentFilterForm,
    CommentForm,
    FilterForm,
    NotificationsForm,
    PhaseForm,
    SortingForm,
)
from .models import (
    Case,
    CaseLog,
    CaseType,
    CaseTypePhase,
    Comment,
    Setting,
    Subscription,
    User,
)

logger = logging.getLogger(__name__)


def login(request):
    if request.POST:
        user = authenticate(
            request,
            username=request.POST["username"],
            password=request.POST["password"],
        )

        if user is not None:
            auth_login(request, user)

    return render(request, "login.html")


def logout(request):
    auth_logout(request)
    return render(request, "logout.html")


@login_required
def notifications(request):
    if request.POST:
        form = NotificationsForm(request.POST, user=request.user)

        if form.is_valid():
            request.user.subscriptions.all().delete()

            for casetype in form.cleaned_data:
                for phase in form.cleaned_data[casetype]:
                    request.user.subscriptions.create(phase=phase)

            messages.success(request, "Instellingen opgeslagen.")

    else:
        form = NotificationsForm(user=request.user)

    return render(request, "notifications.html", {"form": form})


@login_required
def cases(request):
    cases = Case.objects.all()
    filter_form = FilterForm(request.GET)
    sort = False

    if request.GET:
        filter_form = FilterForm(request.GET)
        if filter_form.is_valid():
            data = filter_form.cleaned_data

            if data.get("q"):
                query = SearchQuery(data.get("q"))
                cases = cases.annotate(
                    rank=SearchRank(F("search_vector"), query)
                ).filter(search_vector=query)

            if data.get("casetype"):
                cases = cases.filter(casetype=data.get("casetype"))

            if data.get("manager"):
                cases = cases.filter(manager=data.get("manager"))

            if data.get("responsible"):
                cases = cases.filter(responsible=data.get("responsible"))

            if data.get("status"):
                cases = cases.filter(status=data.get("status"))

            if data.get("tags"):
                cases = cases.filter(tags__contains=data.get("tags"))

            if data.get("phase"):
                cases = cases.filter(current_phase=data.get("phase"))

            if data.get("created_on_start"):
                cases = cases.filter(created_on__gte=data.get("created_on_start"))

            if data.get("created_on_end"):
                cases = cases.filter(created_on__lte=data.get("created_on_end"))

            if data.get("publication_date_start"):
                cases = cases.filter(
                    publication_date__gte=data.get("publication_date_start")
                )

            if data.get("publication_date_end"):
                cases = cases.filter(
                    publication_date__lte=data.get("publication_date_end")
                )

            if data.get("expiration_date_start"):
                cases = cases.filter(
                    expiration_date__gte=data.get("expiration_date_start")
                )

            if data.get("expiration_date_end"):
                cases = cases.filter(
                    expiration_date__lte=data.get("expiration_date_end")
                )

            if data.get("deadline_start"):
                cases = cases.filter(deadline__gte=data.get("deadline_start"))

            if data.get("deadline_end"):
                cases = cases.filter(deadline__lte=data.get("deadline_end"))

        else:
            filter_form = FilterForm()

        sorting_form = SortingForm(request.GET)
        if sorting_form.is_valid():
            data = sorting_form.cleaned_data
            if data.get("sort"):
                sort = data.get("sort")
                cases = cases.order_by(
                    ("-" if data.get("dir") == "asc" else "")
                    +
                    # workaround because the query builder doesnt use id on current_phase automatically (see model settings -> meta -> order)
                    (sort if sort != "current_phase" else "current_phase__id")
                ).distinct("id", sort)
        else:
            cases = cases.distinct("id")

    paginator = Paginator(cases, 50)

    try:
        page = paginator.page(request.GET.get("page"))
    except PageNotAnInteger:
        page = paginator.page(1)
    except EmptyPage:
        page = paginator.page(paginator.num_pages)

    index = page.number - 1
    page_range = paginator.page_range[
        max(0, index - 5) : (min(len(paginator.page_range), index + 5))
    ]

    # TODO: maybe always creating the Setting singleton on site creation is an improvement
    all_filters_enabled = True
    try:
        all_filters_enabled = len(
            Setting._meta.get_field("shown_filters").default()
        ) == len(Setting.objects.all().first().shown_filters)
    except:
        pass

    return render(
        request,
        "cases.html",
        {
            "filter_form": filter_form,
            "page_range": page_range,
            "page": page,
            "show_sidebar": True,
            "all_filters_enabled": all_filters_enabled,
        },
    )


@login_required
def add_case(request):
    if request.POST:
        form = CaseForm(request.POST)

        for file in request.FILES.getlist("attachments"):
            try:
                validate_file_type(file)
            except:
                messages.error(request, "Bestandstype wordt niet ondersteund")
                return render(request, "add_case.html", {"form": form})

        if form.is_valid():
            case = form.save(commit=False)

            possible_phases = case.casetype.phases.all()
            if possible_phases:
                case.current_phase = possible_phases[0]

            case.save()

            for file in request.FILES.getlist("attachments"):
                attachment = case.attachments.create(content_object=case, file=file)
                scan_file(request, attachment, file, delete_if_virus=True)

            case.logs.create(performed_by=request.user, event="created")
            messages.success(request, "Item is toegevoegd.")

            return redirect("/cases/view/{0}".format(case.id))
    else:
        form = CaseForm()

    return render(request, "add_case.html", {"form": form})


@login_required
def edit_case(request, id):
    case = get_object_or_404(Case, pk=id)

    if request.POST:
        form = CaseForm(request.POST, instance=case)

        for file in request.FILES.getlist("attachments"):
            try:
                validate_file_type(file)
            except:
                messages.error(request, "Bestandstype wordt niet ondersteund")
                return redirect("/cases/edit/{0}".format(case.id))

        if form.is_valid():
            form.save()

            for file in request.FILES.getlist("attachments"):
                attachment = case.attachments.create(content_object=case, file=file)
                scan_file(request, attachment, file, delete_if_virus=True)

            case.logs.create(performed_by=request.user, event="updated")
            messages.success(request, "Item is gewijzigd.")

            return redirect("/cases/view/{0}".format(case.id))
    else:
        form = CaseForm(instance=case)

    return render(request, "edit_case.html", {"form": form, "case": case})


@login_required
def delete_case(request, id):
    case = get_object_or_404(Case, pk=id)

    if request.POST:
        if request.POST.get("id") == request.POST.get("confirm_id"):
            case.delete()
            messages.success(request, "Item is verwijderd.")
            return redirect("/")
        else:
            messages.warning(request, "Je hebt het itemnummer niet goed ingevuld.")

    return render(request, "delete_case.html", {"case": case})


@login_required
def view_case(request, id):
    case = get_object_or_404(Case, pk=id)

    internal_comments = case.comments.all()
    external_comments = case.external_comments.all()

    logs = case.logs.filter(event="changed_phase")

    manager = case.casetype.manager()

    if request.GET:
        comment_filter_form = CommentFilterForm(request.GET)
    else:
        comment_filter_form = CommentFilterForm()

    if request.GET.get("comment_type") == "external":
        comments = chain(external_comments, logs)
    elif request.GET.get("comment_type") == "all":
        comments = chain(internal_comments, external_comments, logs)
    else:
        comments = chain(internal_comments, logs)

    return render(
        request,
        "view_case.html",
        {
            "case": case,
            "timeline": sorted(
                comments, key=lambda instance: instance.created_on, reverse=True
            ),
            "is_manager": case.manager == request.user,
            "comment_filter_form": comment_filter_form,
            "comment_form": CommentForm(),
            "phase_form": PhaseForm(
                casetype=case.casetype,
                tasks=[
                    (i, t.label) for i, t in enumerate(manager.tasks)
                ],
                closing_tasks=[
                    (i, t.label) for i, t in enumerate(manager.closing_tasks)
                ]
            ),
            "delete_case_disabled": settings.DELETE_CASE_DISABLED,
        },
    )


@login_required
def view_case_logs(request, id):
    case = get_object_or_404(Case, pk=id)

    return render(
        request, "view_case_logs.html", {"case": case, "comment_form": CommentForm()}
    )


@login_required
def claim_case(request, id):
    if request.POST:
        case = get_object_or_404(Case, pk=id)

        if case.claim(request.user):
            messages.success(request, "Je bent nu de behandelaar van dit item.")
        else:
            messages.error(request, "Iemand anders behandelt dit item al.")

    return redirect("/cases/view/{0}".format(case.id))


@login_required
def unclaim_case(request, id):
    case = get_object_or_404(Case, pk=id)

    if request.POST:
        if case.unclaim(request.user):
            messages.success(request, "De behandelaar is van dit item afgehaald.")
        else:
            messages.error(request, "Dit item heeft momenteel geen behandelaar.")

    return redirect("/cases/view/{0}".format(case.id))


@login_required
def mark_closed(request, id):
    if request.POST:
        case = get_object_or_404(Case, pk=id)

        if case.mark_closed(request.user):
            messages.success(request, 'De zaak is gemarkeerd als "gesloten".')
        else:
            messages.error(
                request,
                'De zaak heeft niet de status "in afwachting" dus kan niet gesloten worden.',
            )

    return redirect("/cases/view/{0}".format(case.id))


@login_required
def remove_responsible(request, id):
    case = get_object_or_404(Case, pk=id)

    if case.remove_responsible(request.user):
        messages.success(request, "De verantwoordelijke is van dit item afgehaald.")
    else:
        messages.error(request, "Dit item heeft geen verantwoordelijke.")

    return redirect("/cases/view/{0}".format(case.id))


@login_required
def add_comment(request):
    case = get_object_or_404(Case, pk=request.POST.get("case_id"))
    form = CommentForm(request.POST)
    manager = case.casetype.manager()

    if not request.POST:
        return redirect("/cases/view/{0}".format(case.id))
    
    for file in request.FILES.getlist("attachments"):
            try:
                validate_file_type(file)
            except:
                messages.error(request, "Bestandstype wordt niet ondersteund")
                return redirect("/cases/view/{0}".format(case.id))

    if form.is_valid():
        comment = form.save(commit=False)
        comment.author = request.user
        comment.case = case
        comment.save()
        for file in request.FILES.getlist("attachments"):
            attachment = comment.attachments.create(content_object=comment, file=file)
            scan_file(request, attachment, file, delete_if_virus=True)

        case.logs.create(performed_by=request.user, event="commented")

        if request.POST.get("tasks"):
            for i in request.POST.get("tasks"):
                task = manager.tasks[int(i)]
                if task:
                    task.on_change(case, comment, request)  # execute task

        if request.POST.get("phase"):
            new_phase = get_object_or_404(CaseTypePhase, pk=request.POST.get("phase"))
            
            if new_phase is not case.current_phase:
                case.change_phase(new_phase, request.user)

                if request.POST.get("closing_tasks"):
                    for i in request.POST.get("closing_tasks"):
                        task = manager.closing_tasks[int(i)]
                        if task:
                            task.on_close(case, comment, request)  # execute task

                subscriptions = Subscription.objects.filter(phase=new_phase)

                for subscription in subscriptions:
                    parameters = {
                        "user": subscription.user,
                        "case": case,
                        "new_phase": new_phase,
                        "url": request.build_absolute_uri(
                            reverse("view_case", kwargs={"id": case.id})
                        ),
                    }

                    send_mail(
                        render_to_string(
                            "emails/phase_changed_subject.txt", parameters
                        ),
                        render_to_string("emails/phase_changed.txt", parameters),
                        settings.EMAIL_FROM,
                        [formataddr((subscription.user.name, subscription.user.email))],
                        fail_silently=True,
                    )

        if request.POST.get("responsible"):
            new_responsible = get_object_or_404(
                User, pk=request.POST.get("responsible")
            )
            case.responsible = new_responsible
            case.save()
            case.logs.create(
                performed_by=request.user,
                event="changed_responsible",
                to=new_responsible.name,
            )

        return redirect("/cases/view/{0}".format(case.id))

    else:
        comments = case.comments.all()
        logs = case.logs.filter(event="changed_phase")

        return render(
            request,
            "view_case.html",
            {
                "case": case,
                "timeline": sorted(
                    chain(comments, logs), 
                    key=lambda instance: instance.created_on, 
                    reverse=True
                ),
                "is_manager": case.manager == request.user,
                "comment_form": form,
                "phase_form": PhaseForm(
                    casetype=case.casetype,
                    tasks=[
                        (i, t.label) for i, t in enumerate(manager.tasks)
                    ],
                    closing_tasks=[
                        (i, t.label) for i, t in enumerate(manager.closing_tasks)
                    ]
                ),
            },
        )


@login_required
def edit_comment(request, id):

    def publish_edit_comment(comment):
        casetype = comment.case.casetype

        if not casetype.settings.get("pleio_url"):
            return

        if not casetype.settings.get("pleio_access_token"):
            return

        url = urljoin(casetype.settings["pleio_url"], "flow/comments/edit")

        if not comment.external_id:
            return logger.warning(
                "Could not log case {} as it does not have an external_id".format(
                    comment.case.id
                )
            )

        data = {
            "container_guid": comment.external_id,
            "description": comment.description,
        }

        headers = {
            "Authorization": "Bearer {}".format(casetype.settings["pleio_access_token"])
        }

        files = get_opened_files(comment.attachments.all())

        try:
            requests.post(url, data=data, headers=headers, files=files, timeout=15)
        except:
            pass

        close_files(files)

    comment = get_object_or_404(Comment, pk=id)

    if not comment.can_edit(request.user):
        return HttpResponseForbidden()

    if request.POST:
        for file in request.FILES.getlist("attachments"):
            try:
                validate_file_type(file)
            except:
                messages.error(request, "Bestandstype wordt niet ondersteund")
                return redirect("/comments/edit/{0}".format(comment.id))
        
        form = CommentForm(request.POST, instance=comment)
        if form.is_valid():
            form.save()

            for file in request.FILES.getlist("attachments"):
                attachment = comment.attachments.create(content_object=comment, file=file)
                scan_file(request, attachment, file, delete_if_virus=True)

            if comment.external_id:
                publish_edit_comment(comment)

            return redirect("/cases/view/{0}".format(comment.case.id))
    else:
        form = CommentForm(instance=comment)

    return render(request, "edit_comment.html", {"comment": comment, "form": form})


@login_required
def delete_comment(request, id):
    comment = get_object_or_404(Comment, pk=id)

    if not comment.can_edit(request.user):
        return HttpResponseForbidden()

    if request.POST:
        comment.delete()
        return redirect("/cases/view/{0}".format(comment.case.id))

    return render(request, "delete_comment.html", {"comment": comment})


@login_required
def protected_media(request, path):
    media_root = settings.MEDIA_ROOT

    file_path = os.path.join(media_root, path)
    absolute_path = os.path.abspath(file_path)

    if not absolute_path.startswith(media_root + os.path.sep):
        raise Http404

    if os.path.exists(absolute_path):
        return FileResponse(open(absolute_path, "rb"))
    else:
        raise Http404
