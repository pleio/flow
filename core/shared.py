from django.core.files.storage import default_storage
from django.contrib import messages


def scan_file(request, file, file_name, delete_if_virus=False, delete_from_disk=False):
    if not file.scan():
        if delete_from_disk:
            default_storage.delete(file.file.name)
        if delete_if_virus:
            file.delete()
        messages.error(
            request,
            "Het bestand '{}' is geweigerd omdat het mogelijk een virus bevat. Controleer het bestand en probeer het opnieuw.".format(
                file_name
            ),
        )
