import csv
import os

from django.contrib import admin, messages
from django.template.response import TemplateResponse
from django.http import StreamingHttpResponse, HttpResponseRedirect
from django.urls import path
from django.utils.html import format_html
from rangefilter.filters import DateRangeFilter
from solo.admin import SingletonModelAdmin

from core.lib import get_cases_list_for_export
from core.tasks import create_users_export, create_cases_export

from .models import (
    Case, 
    CaseType, 
    CaseTypePhase, 
    CsvExport, 
    Setting, 
    Subscription, 
    User,
    ScanIncident, 
)

class Echo:
    """An object that implements just the write method of the file-like
    interface.
    """

    def write(self, value):
        """Write the value by returning it, instead of storing in a buffer."""
        return value



def export_cases(casetypemodel, request, queryset):
    cases = get_cases_list_for_export(queryset)

    # return csv
    pseudo_buffer = Echo()
    writer = csv.writer(pseudo_buffer, delimiter=";")
    response = StreamingHttpResponse(
        (writer.writerow(row) for row in cases), content_type="text/csv"
    )
    response["Content-Disposition"] = 'attachment; filename="cases.csv"'
    return response


export_cases.short_description = "Exporteer geselecteerde cases"


class CaseTypeAdmin(admin.ModelAdmin):
    fieldsets = (
        ("Technische eigenschappen", {"fields": ("name", "type", "settings", "initial_notification")}),
        (
            "Automatisch antwoord",
            {
                "description": "De autoresponder wordt ingeschakeld als zowel een start datum/tijd als een bericht zijn ingesteld. De autoresponder stopt na de vervaldatum.",
                "fields": (
                    "autoresponder_start",
                    "autoresponder_due",
                    "autoresponder_message",
                ),
            },
        ),
    )

class CaseTypePhaseAdmin(admin.ModelAdmin):
    list_display = ("casetype", "name", "order", "max_duration_hours")


class SubscriptionAdmin(admin.ModelAdmin):
    list_display = ("user", "phase")


class SettingAdmin(SingletonModelAdmin):
    list_display = ("shown_columns",)

class ScanIncidentAdmin(admin.ModelAdmin):
    def has_add_permission(self, request, obj=None):
        return False

    def has_change_permission(self, request, obj=None):
        return False

    list_display = ("file_created", "file_title", "message", "is_virus")

class CaseAdmin(admin.ModelAdmin):
    list_display = ("name",)
    list_filter = (
        ("created_on", DateRangeFilter),
        "responsible",
    )

class CsvExportAdmin(admin.ModelAdmin):
    change_list_template = "../templates/change_list.html"
    
    @admin.action(description="Exporteer users in achtergrond")
    def export_users_background(self, request):
        if request.POST.get('post'):
            queryset = User.objects.all()
            user_ids = list(queryset.values_list("id", flat=True))
            create_users_export.delay(user_ids, request.user.id)
            self.message_user(
                request,
                "Exporteren van users in de achtergrond gestart",
                messages.SUCCESS,
            )
            return HttpResponseRedirect("../")
        else:
            response = TemplateResponse(request, '../templates/confirm_export.html')
            return response


    @admin.action(description="Exporteer cases in achtergrond")
    def export_cases_background(self, request):
        if request.POST.get('post'):
            queryset = Case.objects.all()
            case_ids = list(queryset.values_list("id", flat=True))
            create_cases_export.delay(case_ids, request.user.id)
            self.message_user(
                request,
                "Exporteren van cases in de achtergrond gestart",
                messages.SUCCESS,
            )
            return HttpResponseRedirect("../")
        else:
            response = TemplateResponse(request, '../templates/confirm_export.html')
            return response

    def get_urls(self):
        urls = super().get_urls()
        my_urls = [
            path('export_users/', self.export_users_background),
            path('export_cases/', self.export_cases_background),
        ]
        return my_urls + urls

    list_display = ["file_name", "created_by", "created_on", "download_file"]

    def download_file(self, obj):
        url = obj.csv_file.url
        return format_html('<a href="{}">Download</a>', url)

    download_file.short_description = "Download File"

    def file_name(self, obj):
        return os.path.basename(obj.csv_file.name).split("/")[-1]


admin.site.register(CaseTypePhase, CaseTypePhaseAdmin)
admin.site.register(Subscription, SubscriptionAdmin)
admin.site.register(Setting, SettingAdmin)
admin.site.register(ScanIncident, ScanIncidentAdmin)

admin.site.register(CaseType, CaseTypeAdmin)
admin.site.register(Case, CaseAdmin)
admin.site.register(CsvExport, CsvExportAdmin)
"""
titel: case.name
url: request.build_absolute_uri(reverse('view_case', kwargs={'id': case.id}))
fases: case.casetype.phases
users case.logs.performed_by
"""
