import bleach
from bleach.css_sanitizer import CSSSanitizer
from django.conf import settings
from rest_framework import serializers

from .models import Case, CaseLog, CaseType, ExternalComment


class SanitizedHTMLField(serializers.Field):
    def to_representation(self, text):
        return text

    def to_internal_value(self, text):
        css_sanitizer = CSSSanitizer(
            allowed_css_properties=settings.BLEACH_ALLOWED_STYLES
        )
        return bleach.clean(
            text,
            tags=settings.BLEACH_ALLOWED_TAGS,
            attributes=settings.BLEACH_ALLOWED_ATTRIBUTES,
            css_sanitizer=css_sanitizer,
            strip=True,
        )


class CaseSerializer(serializers.ModelSerializer):
    description = SanitizedHTMLField()

    class Meta:
        model = Case
        fields = (
            "id",
            "casetype",
            "name",
            "description",
            "tags",
            "created_on",
            "external_id",
            "external_author",
            "external_email"
        )


class CaseTypeSerializer(serializers.ModelSerializer):
    class Meta:
        model = CaseType
        fields = ("id", "name")


class CaseLogSerializer(serializers.ModelSerializer):
    performed_by = serializers.HiddenField(default=serializers.CurrentUserDefault())

    class Meta:
        model = CaseLog
        fields = ("id", "performed_by", "event", "case")


class ExternalCommentSerializer(serializers.ModelSerializer):
    description = SanitizedHTMLField()

    class Meta:
        model = ExternalComment
        fields = ("id", "author", "description", "case")

    def create(self, validated_data):
        external_comment = ExternalComment.objects.create(**validated_data)
        case = external_comment.case

        if case.status == "CLOSED":
            case.status = Case.PENDING
            case.save()

        return external_comment
