# Generated by Django 2.2.10 on 2022-03-09 08:36

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0024_auto_20220309_0842'),
    ]

    operations = [
        migrations.AlterField(
            model_name='casetype',
            name='autoresponder_due',
            field=models.DateTimeField(blank=True, null=True, verbose_name='Vervalt na'),
        ),
        migrations.AlterField(
            model_name='casetype',
            name='autoresponder_message',
            field=models.TextField(blank=True, null=True, verbose_name='Berichttekst'),
        ),
        migrations.AlterField(
            model_name='casetype',
            name='autoresponder_start',
            field=models.DateTimeField(blank=True, null=True, verbose_name='Begint op'),
        ),
    ]
