from rest_framework import viewsets
from .models import Case, CaseType, CaseLog, ExternalComment, Setting
from .serializers import CaseSerializer, CaseTypeSerializer, CaseLogSerializer, ExternalCommentSerializer


class CaseViewSet(viewsets.ModelViewSet):
    queryset = Case.objects.all()
    serializer_class = CaseSerializer

    def perform_create(self, serializer):
        case = serializer.save()

        if case.casetype.initial_notification:
            setting = Setting.objects.get()
            case.casetype.manager().handle_notification(case, setting.initial_notification_text)


class CaseTypeViewSet(viewsets.ModelViewSet):
    queryset = CaseType.objects.all()
    serializer_class = CaseTypeSerializer


class CaseLogViewSet(viewsets.ModelViewSet):
    queryset = CaseLog.objects.all()
    serializer_class = CaseLogSerializer


class ExternalCommentViewSet(viewsets.ModelViewSet):
    queryset = ExternalComment.objects.all()
    serializer_class = ExternalCommentSerializer
