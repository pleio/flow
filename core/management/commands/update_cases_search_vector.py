from django.core.management.base import BaseCommand, CommandError
from core.models import Case
import sys

class Command(BaseCommand):
    help = 'Updates the search_vector of all cases'

    def handle(self, *args, **options):
        count = 0
        total = Case.objects.count()
        for case in Case.objects.all():
            case.save()
            count += 1
            self.stdout.write('%s cases updated of %s' % (count, total))
        self.stdout.write(self.style.SUCCESS('Successfully updated seach_vector of all cases'))