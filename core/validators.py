def validate_file_type(value):
    from django.core.exceptions import ValidationError
    import magic
    
    filetype = magic.from_buffer(value.read())

    valid_files = [
        "PNG",
        "JPEG",
        "JPG",
        "PDF",
        "Word",
        "Excel",
        "PowerPoint",
        "OpenDocument",
        "image",
        "Audio",
        "Video",
        "MP3" "MP4",
        "XML",
        "CSV",
        "ASCII text, with CRLF line terminators",
    ]

    if value.name.endswith(("docm", "pptm", "xlsm")) or not any(
        type in filetype for type in valid_files
    ):
        raise ValidationError("Invalid file type.")