import os
import mimetypes

from datetime import timedelta
from importlib import import_module
from itertools import chain
from zoneinfo import ZoneInfo
from math import ceil

from django.apps import apps
from django.conf import settings
from django.core.exceptions import ImproperlyConfigured
from django.utils import timezone
from django.utils.html import strip_tags


class CloseTaskBase:
    label = None

    @classmethod
    def on_close(self, case, comment, request):
        pass

class TaskBase:
    label = None

    @classmethod
    def on_change(self, case, comment, request):
        pass


class BaseCaseTypeWrapper:
    tasks = []
    closing_tasks = []

    def handle_autoresponder(self, case):
        pass

    def handle_notification(self, case):
        pass

class CaseTypeManager:
    def __init__(self):
        self.casetypes = {
            k: self.initialize_case_type(k, v) for k, v in settings.CASE_TYPES.items()
        }

    def initialize_case_type(self, key, settings):
        try:
            backend = import_module("%s.base" % settings["ENGINE"]).CaseTypeWrapper
            return backend(key)
        except ImportError as e_user:
            raise ImproperlyConfigured(
                "%s is not a valid case type.\n"
                "Please check core.casetypes.XXX and verify the name."
                % (settings["ENGINE"])
            ) from e_user

    def options(self):
        return [(k, k) for k in self.casetypes.keys()]

def get_users_list_for_export(queryset):
    headers = [
        "name",
        "email",
        "is_active",
        "is_admin",
        "laatste aanmelding",
        "subscriptions",
    ]
    users = [headers]

    for user in queryset:
        users.append(
            [
                user.name,
                user.email,
                user.is_active,
                user.is_admin,
                user.last_login,
                list(user.subscriptions.all().values_list("phase__name", flat=True)),
            ]
        )
    
    return users

def get_cases_list_for_export(queryset):
    def get_local_datetime(utc_datetime):
        utc_datetime = utc_datetime.replace(microsecond=0)
        local_dt = utc_datetime.astimezone(ZoneInfo("Europe/Amsterdam"))
        return local_dt.replace(tzinfo=None)

    def get_duration(start, end=None):
        if not end:
            end = timezone.now()
        difference = end - start
        return "%sd %s" % (
            str(difference.days),
            str(timedelta(seconds=difference.seconds)),
        )

    def get_comment(comment):
        if comment.__class__.__name__ == "CaseLog":
            return "De fase is veranderd naar %s\n%s" % (
                comment.to,
                get_local_datetime(comment.created_on),
            )
        elif comment.__class__.__name__ == "ExternalComment":
            return "%s (Externe reactie)\n%s\n%s" % (
                comment.author,
                get_local_datetime(comment.created_on),
                comment.description,
            )
        elif comment.__class__.__name__ == "Comment":
            return "%s\n%s\n%s" % (
                comment.author.name,
                get_local_datetime(comment.created_on),
                comment.description,
            )

    def get_comments_export(comment_lists):
        comments_export = []
        for comment in comment_lists[0]:
            comments_export.append(strip_tags(get_comment(comment)))
        return "\n\n".join(comments_export)

    Setting = apps.get_model("core", "Setting")
    base_url = Setting.get_solo().base_url
    headers = [
        "name",
        "url",
        "users",
        "created_on",
        "responsible",
        "current_phase",
        "passed_phases",
        "current_phase_id",
        "passed_phase_ids",
        "time_in_current_phase",
        "time_in_passed_phase_ids",
        "time_total",
        "case",
        "timeline",
    ]
    cases = [headers]

    for case in queryset:
        users = []
        phases = []
        phase_ids = []
        current_phase_name = ""
        current_phase_id = "0"
        time_in_passed_phase_ids = []
        next_log = None
        time_total = get_duration(case.created_on, end=case.closed_on)

        users = case.logs.all().values_list("performed_by__name", flat=True)
        for log in case.logs.filter(event="changed_phase").order_by("created_on"):
            if log.from_phase:
                phases.append(log.from_phase.name)
                phase_id_timestamp = get_local_datetime(log.created_on)
                phase_ids.append(
                    "%i (%s)" % (log.from_phase.id, phase_id_timestamp),
                )
                if next_log:
                    duration = get_duration(next_log.created_on, log.created_on)
                else:
                    duration = get_duration(log.created_on)
                time_in_passed_phase_ids.append(
                    "%i (%s)" % (log.from_phase.id, duration)
                )
            next_log = log

        users = ", ".join(list(set(users)))
        phases = ", ".join(phases)
        phase_ids = ", ".join(phase_ids)
        time_in_passed_phase_ids = ", ".join(time_in_passed_phase_ids)

        if case.current_phase:
            current_phase_name = case.current_phase.name
            last_log = (
                case.logs.filter(event="changed_phase").order_by("created_on").last()
            )
            if last_log:
                timestamp = get_local_datetime(last_log.created_on)
                time_in_current_phase = get_duration(last_log.created_on)
            else:
                timestamp = get_local_datetime(case.created_on)
                time_in_current_phase = get_duration(case.created_on)

            current_phase_id = "%i (%s)" % (
                case.current_phase.id,
                timestamp,
            )

        else:
            current_phase_id_timestamp = get_local_datetime(case.created_on)
            current_phase_id = "%s (%s)" % (
                current_phase_id,
                current_phase_id_timestamp,
            )
            time_in_current_phase = get_duration(case.created_on)

        url = "%s/%s" % (base_url, case.url)
        responsible_name = case.responsible.name if case.responsible else ""
        created_on = get_local_datetime(case.created_on)

        internal_comments = case.comments.all()
        external_comments = case.external_comments.all()

        logs = case.logs.filter(event="changed_phase")

        comments = chain(internal_comments, external_comments, logs)
        comment_lists = (
            sorted(comments, key=lambda instance: instance.created_on, reverse=True),
        )
        timeline = get_comments_export(comment_lists)

        cases.append(
            [
                case.name,
                url,
                users,
                created_on,
                responsible_name,
                current_phase_name,
                phases,
                current_phase_id,
                phase_ids,
                time_in_current_phase,
                time_in_passed_phase_ids,
                time_total,
                strip_tags(case.description),
                timeline,
            ]
        )

    return cases


def get_opened_files(files_list):
    files = {}
    MAX_FILE_SIZE = 20 * 1024 * 1024  # 20 MB in bytes

    for idx, obj in enumerate(files_list):
        file_path = obj.file.path
        file_size = os.path.getsize(file_path)

        # Only allow files smaller than 20 MB
        if file_size <= MAX_FILE_SIZE:
            file_key = f"file{idx}"
            files[file_key] = (
                obj.file.name,
                open(file_path, "rb"),
                "multipart/form-data",
            )
        return files


def close_files(files):
    if files:
        for file_tuple in files.values():
            try:
                file_tuple[1].close()
            except Exception:
                pass

def get_mimetype(filepath):
    mimetypes.init()
    mime_type, _ = mimetypes.guess_type(filepath)
    if not mime_type:
        return None
    return mime_type

class Distribute:
    """
    Given a list of items.

    Mix the left side of the list with the right side of the list so that neighbours end with a
    distance of approximately 'distance', and are mixed with the left side.
    """

    def __init__(self, content, distance):
        self.content = content or []
        self.distance = distance

    def spread(self):
        # Continue until all is done.
        if len(self.content) == 0:
            return

        # Step one: collect a linear-distributed subset from the full list.
        step = ceil(len(self.content) / self.distance)
        sub_set = []
        for n in reversed(range(0, self.distance)):
            try:
                sub_set.insert(0, self.content.pop(step * n))
            except IndexError:
                continue

        # Step 2: Mix them utter right, utter left, etc.
        while len(sub_set) > 0:
            try:
                yield sub_set.pop(0)
                yield sub_set.pop(len(sub_set) - 1)
            except IndexError:
                break

        # Recursive operation.
        yield from self.spread()