from django.conf import settings
from django.core.mail import send_mail

from core.lib import BaseCaseTypeWrapper
from core.lib import CloseTaskBase
from django.contrib import messages


class ShowMessage(CloseTaskBase):
    label = 'Toon een berichtje op de volgende pagina'

    @classmethod
    def on_close(cls, case, comment, request):
        messages.info(request, comment.description)


class SendTestmail(CloseTaskBase):
    label = 'Stuur een berichtje aan jezelf'

    @classmethod
    def on_close(cls, case, comment, request):
        send_mail(subject=f"Bevestiging afsluiten {case.name}",
                  message=comment.description,
                  from_email=settings.EMAIL_FROM,
                  recipient_list=[comment.author.email],
                  fail_silently=False)


class CaseTypeWrapper(BaseCaseTypeWrapper):
    name = 'testmail'

    closing_tasks = [
        ShowMessage,
        SendTestmail
    ]

    def __init__(self, key):
        self.key = key
