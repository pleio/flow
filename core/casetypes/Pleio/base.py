import logging

from django.core.mail import send_mail
from django.template.loader import render_to_string
from email.utils import formataddr
from django.conf import settings

from core.casetypes.Pleio.connector import PleioConnector
from core.lib import BaseCaseTypeWrapper, TaskBase, close_files, get_opened_files

logger = logging.getLogger(__name__)


class PublishToPleio(TaskBase):
    label = "Publiceer naar de voorkant"

    @classmethod
    def on_change(cls, case, comment, request):
        try:
            if not case.external_id:
                logger.warning(
                    "Could not log case {} as it does not have an external_id".format(
                        case.id
                    )
                )
                return

            files = get_opened_files(comment.attachments.all())

            for idx, obj in enumerate(comment.attachments.all()):
                file_path = obj.file.path

                file_key = f"file{idx}"
                files[file_key] = (
                    obj.file.name,
                    open(file_path, "rb"),
                    "multipart/form-data",
                )

            connector = PleioConnector(case)
            connector.send_feedback(comment.description, files)

            comment.external_id = connector.response["comment_id"]
            comment.save()

            close_files(files)

        except Exception as e:
            logger.warning(msg="@publish_frontend: {}".format(str(e)))


def handle_autoresponder(case):
    try:
        connector = PleioConnector(case)
        connector.send_feedback(case.casetype.autoresponder_message)
    except Exception as e:
        logger.warning(msg="@handle_autoresponder: {}".format(str(e)))

def handle_notification(case, initial_notification_text):
    parameters = {
        "user": case.external_author,
        "case": case
    }
    send_mail(
        render_to_string("emails/initial_notification_subject.txt", parameters),
        initial_notification_text,
        settings.EMAIL_FROM,
        [formataddr((case.external_author, case.external_email))],
        fail_silently=True,
    )

class CaseTypeWrapper(BaseCaseTypeWrapper):
    name = "pleio"

    tasks = [PublishToPleio]

    def handle_autoresponder(self, case):
        handle_autoresponder(case)

    def handle_notification(self, case, initial_notification_text):
        handle_notification(case, initial_notification_text)

    def __init__(self, key):
        self.key = key
