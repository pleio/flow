from core.lib import BaseCaseTypeWrapper

class CaseTypeWrapper(BaseCaseTypeWrapper):
    name = 'simple'

    def __init__(self, key):
        self.key = key
