from __future__ import unicode_literals

import json
import os
import uuid

from django import forms
from django.contrib.contenttypes.fields import GenericForeignKey, GenericRelation
from django.contrib.contenttypes.models import ContentType
from django.contrib.postgres.aggregates import StringAgg
from django.contrib.postgres.fields import ArrayField
from django.contrib.postgres.indexes import GinIndex
from django.contrib.postgres.search import SearchVector, SearchVectorField
from django.db import models
from django.db.models.signals import post_delete, post_save, pre_save
from django.dispatch import receiver
from django.urls import reverse
from django.utils import timezone
from django.utils.timezone import localtime
from solo.models import SingletonModel

from core.lib import CaseTypeManager, get_mimetype
from core.utils import clamav
from pleio_auth.models import User

manager = CaseTypeManager()


def _serialize(nk):
    return json.dumps(nk)


def _materialize(data):
    return json.loads(data)


class MultipleArrayField(ArrayField):
    def formfield(self, **kwargs):
        defaults = {
            "form_class": forms.MultipleChoiceField,
            "choices": self.base_field.choices,
            "widget": forms.CheckboxSelectMultiple,
            **kwargs,
        }
        return super(ArrayField, self).formfield(**defaults)


class Setting(SingletonModel):
    def default_show_columns():
        return [
            "id",
            "name",
            "status",
            "created_on",
            "latest_phase_change",
            "casetype",
            "current_phase",
            "expiration_date",
            "deadline",
            "responsible",
            "tags",
        ]

    def default_show_filters():
        return [
            "status",
            "created_on",
            "casetype",
            "phase",
            "deadline",
            "responsible",
            "tags",
        ]

    COLUMN_NAMES = (
        ("id", "ID"),
        ("name", "Naam"),
        ("status", "Status"),
        ("created_on", "Startdatum"),
        ("latest_phase_change", "Looptijd"),
        ("casetype", "Type"),
        ("current_phase", "Fase"),
        ("publication_date", "Publicatiedatum"),
        ("expiration_date", "Vervaldatum"),
        ("deadline", "Deadline"),
        ("responsible", "Verantwoordelijke"),
        ("tags", "Tags"),
    )

    FILTER_NAMES = (
        ("status", "Status"),
        ("casetype", "Type"),
        ("phase", "Fase"),
        ("responsible", "Verantwoordelijke"),
        ("created_on", "Startdatum"),
        ("publication_date", "Publicatiedatum"),
        ("expiration_date", "Vervaldatum"),
        ("deadline", "Deadline"),
        ("tags", "Tags"),
    )
    shown_columns = MultipleArrayField(
        models.CharField(choices=COLUMN_NAMES, max_length=100, blank=False, null=True),
        default=default_show_columns,
        blank=False,
        null=False,
    )
    shown_filters = MultipleArrayField(
        models.CharField(choices=FILTER_NAMES, max_length=100, blank=True, null=True),
        blank=True,
        default=default_show_filters,
        null=True,
    )
    base_url = models.CharField(max_length=300, default="", blank=True, null=True)
    initial_notification_text = models.TextField(max_length=500, default="Item ontvangen", 
                                                 blank=True, null=True)

    def __unicode__(self):
        return "Settings"

    class Meta:
        verbose_name = "Settings"


class Subscription(models.Model):
    user = models.ForeignKey(
        User, related_name="subscriptions", on_delete=models.CASCADE
    )
    phase = models.ForeignKey("CaseTypePhase", on_delete=models.CASCADE)


class CaseTypeManager(models.Manager):
    def get_by_natural_key(self, name):
        return self.get(name=name)


class CaseType(models.Model):
    objects = CaseTypeManager()

    name = models.CharField(max_length=100, unique=True)
    type = models.CharField(max_length=100, choices=manager.options(), default="simple")
    settings = models.JSONField(default=dict, blank=True)

    autoresponder_start = models.DateTimeField(
        null=True, blank=True, verbose_name="Start op"
    )
    autoresponder_due = models.DateTimeField(
        null=True, blank=True, verbose_name="Vervalt na"
    )
    autoresponder_message = models.TextField(
        null=True, blank=True, verbose_name="Berichttekst"
    )
    initial_notification = models.BooleanField(
        null=False, blank=False, default=False, verbose_name="Automatische e-mailnotificatie"    
    )

    def natural_key(self):
        return (self.name,)

    @property
    def is_autoresponder_active(self):
        if self.autoresponder_message is None:
            return False
        if self.autoresponder_start is None or self.autoresponder_start >= localtime():
            return False
        if self.autoresponder_due is not None and self.autoresponder_due <= localtime():
            return False
        return True

    def __str__(self):
        return self.name

    def manager(self):
        return manager.casetypes.get(self.type, None)


class CaseTypePhaseManager(models.Manager):
    def get_by_natural_key(self, casetype, name):
        return self.get(casetype__name=casetype, name=name)


class CaseTypePhase(models.Model):
    class Meta:
        ordering = ["casetype", "order", "id"]
        unique_together = [["casetype", "name"]]

    objects = CaseTypePhaseManager()

    casetype = models.ForeignKey(
        "CaseType", related_name="phases", on_delete=models.CASCADE
    )
    name = models.CharField(max_length=100)
    order = models.PositiveIntegerField()
    max_duration_hours = models.DecimalField(
        max_digits=6,
        decimal_places=2,
        null=True,
        blank=True,
        help_text="Notify after number of hours, mo-fr 9:00-17:00",
    )

    def __str__(self):
        return "{} - {}".format(self.casetype.name, self.name)

    def natural_key(self):
        return (self.casetype.name, self.name)

    natural_key.dependencies = ("core.CaseType",)


class UIDNaturalKeyManager(models.Manager):
    def get_by_natural_key(self, natural_key):
        return self.get(uid=natural_key)


class CaseLog(models.Model):
    class Meta:
        ordering = ["-created_on"]

    objects = UIDNaturalKeyManager()

    uid = models.UUIDField(default=uuid.uuid4, unique=True)

    case = models.ForeignKey("Case", related_name="logs", on_delete=models.CASCADE)
    from_phase = models.ForeignKey(
        "CaseTypePhase", null=True, blank=True, on_delete=models.CASCADE
    )
    performed_by = models.ForeignKey(User, on_delete=models.CASCADE)
    event = models.CharField(max_length=100)
    to = models.CharField(max_length=255)
    created_on = models.DateTimeField(auto_now_add=True)

    def natural_key(self):
        return (str(self.uid),)


class TextStringAgg(StringAgg):
    output_field = models.TextField()


class CaseManager(UIDNaturalKeyManager):
    def with_documents(self):
        vector = (
            SearchVector("name", weight="A")
            + SearchVector("description", weight="B")
            + SearchVector("external_author", weight="B")
            + SearchVector(
                TextStringAgg("comments__description", delimiter=" "), weight="C"
            )
            + SearchVector(
                TextStringAgg("comments__author__name", delimiter=" "), weight="C"
            )
            + SearchVector(
                TextStringAgg("external_comments__description", delimiter=" "),
                weight="C",
            )
            + SearchVector(
                TextStringAgg("external_comments__author", delimiter=" "), weight="C"
            )
        )
        return self.get_queryset().annotate(document=vector)


class Case(models.Model):
    OPEN = "OPEN"
    CLOSED = "CLOSED"
    PENDING = "PENDING"

    STATUS_CHOICES = ((OPEN, "Open"), (CLOSED, "Gesloten"), (PENDING, "In afwachting"))

    class Meta:
        ordering = ["-created_on"]
        indexes = (GinIndex(fields=["search_vector"]),)

    objects = CaseManager()

    uid = models.UUIDField(default=uuid.uuid4, unique=True)

    casetype = models.ForeignKey("CaseType", on_delete=models.CASCADE)
    name = models.CharField(max_length=256)
    description = models.TextField(null=True)
    tags = ArrayField(models.CharField(max_length=100), blank=True)
    data = models.JSONField(default=dict)
    manager = models.ForeignKey(
        User, null=True, related_name="managing", on_delete=models.CASCADE
    )
    responsible = models.ForeignKey(
        User, null=True, related_name="responsible_for", on_delete=models.CASCADE
    )
    current_phase = models.ForeignKey(
        "CaseTypePhase", null=True, blank=True, on_delete=models.CASCADE
    )
    external_id = models.CharField(max_length=100, null=True)
    external_author = models.CharField(max_length=100, default="")
    external_email = models.EmailField(max_length=255, null=True)

    is_open = models.BooleanField(default=True)
    status = models.CharField(max_length=10, choices=STATUS_CHOICES, default=OPEN)
    deadline = models.DateTimeField(null=True, blank=True)
    publication_date = models.DateTimeField(null=True, blank=True)
    expiration_date = models.DateTimeField(null=True, blank=True)
    latest_phase_change = models.DateTimeField(auto_now_add=True, null=True)
    notes = models.TextField(null=True, blank=True, verbose_name="Notities")
    max_duration_case_phase_notification_sent = models.BooleanField(default=False)
    attachments = GenericRelation("Attachment")
    created_on = models.DateTimeField(auto_now_add=True)
    closed_on = models.DateTimeField(null=True, blank=True)

    search_vector = SearchVectorField(null=True)

    def __str__(self):
        return self.name

    def natural_key(self):
        return (str(self.uid),)

    @property
    def url(self):
        return reverse("view_case", kwargs={"id": self.id})

    def status_verbose(self):
        return dict(Case.STATUS_CHOICES)[self.status]

    def save(self, *args, **kwargs):
        super().save(*args, **kwargs)
        if (
            "update_fields" not in kwargs
            or "search_vector" not in kwargs["update_fields"]
        ):
            instance = self._meta.default_manager.with_documents().get(pk=self.pk)
            instance.search_vector = instance.document
            instance.save(update_fields=["search_vector"])

    def change_phase(self, new_phase, user):
        casetype_phases = list(self.casetype.phases.all())

        if new_phase not in casetype_phases:
            return False

        if new_phase == self.current_phase:
            return True

        if casetype_phases[-1] == new_phase:
            self.status = Case.CLOSED
            self.closed_on = timezone.now()
            self.latest_phase_change = None
        else:
            self.status = Case.OPEN
            self.closed_on = None
            self.latest_phase_change = timezone.now()
            self.max_duration_case_phase_notification_sent = False

        current_phase = self.current_phase
        self.current_phase = new_phase
        self.manager = None

        self.save()

        self.logs.create(
            performed_by=user,
            from_phase=current_phase,
            event="changed_phase",
            to=new_phase.name,
        )
        return True

    def claim(self, user):
        if self.manager:
            return False

        self.manager = user
        self.save()
        self.logs.create(performed_by=user, event="claimed")
        return True

    def unclaim(self, user):
        if not self.manager:
            return False

        self.manager = None
        self.save()
        self.logs.create(performed_by=user, event="unclaimed")
        return True

    def mark_closed(self, user):
        if self.status == Case.PENDING:
            self.status = "CLOSED"
            self.closed_on = timezone.now()
            self.save()
            self.logs.create(performed_by=user, event="mark_closed")
            return True
        else:
            return False

    def remove_responsible(self, user):
        if not self.responsible:
            return False

        self.responsible = None
        self.save()
        self.logs.create(performed_by=user, event="removed_responsible")
        return True


@receiver(pre_save, sender=Case)
def pre_save_case(sender, instance, **kwargs):
    if not instance.casetype.manager():
        return

    if instance.id is None and instance.casetype.is_autoresponder_active:
        instance.casetype.manager().handle_autoresponder(instance)


class Comment(models.Model):
    class Meta:
        ordering = ["created_on"]

    objects = UIDNaturalKeyManager()

    uid = models.UUIDField(default=uuid.uuid4, unique=True)

    author = models.ForeignKey(User, on_delete=models.CASCADE)
    case = models.ForeignKey("Case", related_name="comments", on_delete=models.CASCADE)
    description = models.TextField(null=True)
    created_on = models.DateTimeField(auto_now_add=True)
    attachments = GenericRelation("Attachment")
    external_id = models.CharField(max_length=100, null=True)

    def natural_key(self):
        return (str(self.uid),)

    def can_edit(self, user):
        if user == self.author:
            return True

        if user.is_admin:
            return True

        return False


class ExternalComment(models.Model):
    class Meta:
        ordering = ["created_on"]

    objects = UIDNaturalKeyManager()

    uid = models.UUIDField(default=uuid.uuid4, unique=True)

    author = models.TextField()
    case = models.ForeignKey(
        "Case", related_name="external_comments", on_delete=models.CASCADE
    )
    description = models.TextField(null=True)
    created_on = models.DateTimeField(auto_now_add=True)

    def natural_key(self):
        return (str(self.uid),)

    def can_edit(self, user):
        if user.is_admin:
            return True

        return False


class Attachment(models.Model):
    objects = UIDNaturalKeyManager()

    object_id = models.PositiveIntegerField()
    content_type = models.ForeignKey(ContentType, on_delete=models.CASCADE)
    content_object = GenericForeignKey("content_type", "object_id")
    file = models.FileField(upload_to="attachments/%Y/%m/%d/")
    uid = models.UUIDField(default=uuid.uuid4, unique=True)
    last_scan = models.DateTimeField(default=timezone.now)
    created_at = models.DateTimeField(default=timezone.now)
    size = models.IntegerField(default=0)

    blocked = models.BooleanField(default=False)
    block_reason = models.CharField(max_length=255, null=True, blank=True)

    def natural_key(self):
        return (str(self.uid),)

    def filename(self):
        return os.path.basename(self.file.name)
    
    def set_size(self):
        self.size = self.file.size
    
    def scan(self):
        try:
            Attachment.objects.filter(object_id=self.object_id).update(last_scan=timezone.now())
            clamav.scan(self.file.name)
            self.set_size()
            return True
        except AttributeError:
            return False
        except clamav.FileScanError as e:
            ScanIncident.objects.create_from_attachment(e, self)
            return not e.is_virus()


class CsvExport(models.Model):
    csv_file = models.FileField(upload_to="csv_exports/")
    created_by = models.ForeignKey(User, on_delete=models.CASCADE)
    created_on = models.DateTimeField(auto_now_add=True)

class ScanIncidentManager(models.Manager):
    def create_from_attachment(self, e, attachment):
        self.create(
            message=e.feedback,
            is_virus=e.is_virus(),
            file_created=attachment.created_at,
            file_title=attachment.file.name,
            file_mime_type=get_mimetype(attachment.file.name),
        )

class ScanIncident(models.Model):
    objects = ScanIncidentManager()

    date = models.DateTimeField(default=timezone.now)
    message = models.CharField(max_length=256)
    file = models.ForeignKey(
        "core.Attachment",
        blank=True,
        null=True,
        on_delete=models.SET_NULL,
        related_name="scan_incidents",
    )
    file_created = models.DateTimeField(default=timezone.now)
    file_title = models.CharField(max_length=256)
    file_mime_type = models.CharField(null=True, blank=True, max_length=100)
    is_virus = models.BooleanField(default=False)

    class Meta:
        ordering = ("-date",)


@receiver(post_save, sender=Comment)
@receiver(post_delete, sender=Comment)
def comment_changed(sender, instance, **kwargs):
    instance.case.save()


@receiver(post_save, sender=ExternalComment)
@receiver(post_delete, sender=ExternalComment)
def external_comment_changed(sender, instance, **kwargs):
    instance.case.save()
