import bleach
from bleach.css_sanitizer import CSSSanitizer
from django.conf import settings
from django.contrib.postgres.fields import ArrayField
from django.contrib.postgres.forms import SimpleArrayField
from django.forms import (
    CharField,
    CheckboxSelectMultiple,
    ChoiceField,
    ClearableFileInput,
    DateField,
    DateInput,
    FileField,
    Form,
    ModelChoiceField,
    ModelForm,
    ModelMultipleChoiceField,
    MultipleChoiceField,
    Select,
    Textarea,
    TextInput,
)
from django.utils import timezone

from pleio_auth.models import User

from .models import Case, CaseType, CaseTypePhase, Comment, Setting


class MultipleArrayField(ArrayField):
    def formfield(self, **kwargs):
        defaults = {
            "form_class": MultipleChoiceField,
            "choices": self.base_field.choices,
            "widget": CheckboxSelectMultiple,
            **kwargs,
        }
        return super(ArrayField, self).formfield(**defaults)


class FilterForm(Form):
    q = CharField(
        max_length=100,
        required=False,
        label="Zoekterm",
        widget=TextInput(attrs={"placeholder": "Zoekterm...", "class": "form-control"}),
    )
    status = ChoiceField(
        choices=(
            (None, "---------"),
            ("OPEN", "Open"),
            ("CLOSED", "Gesloten"),
            ("PENDING", "In afwachting"),
        ),
        required=False,
        label="Status",
        widget=Select(attrs={"class": "form-control"}),
    )
    casetype = ModelChoiceField(
        queryset=CaseType.objects,
        required=False,
        label="Type",
        widget=Select(attrs={"class": "form-control"}),
    )
    phase = ModelChoiceField(
        queryset=CaseTypePhase.objects,
        required=False,
        label="Fase",
        widget=Select(attrs={"class": "form-control"}),
    )
    manager = ModelChoiceField(
        queryset=User.objects,
        required=False,
        label="Behandelaar",
        widget=Select(attrs={"class": "form-control"}),
    )
    responsible = ModelChoiceField(
        queryset=User.objects,
        required=False,
        label="Verantwoordelijke",
        widget=Select(attrs={"class": "form-control"}),
    )
    tags = SimpleArrayField(
        CharField(max_length=100),
        required=False,
        label="Tags",
        widget=TextInput(attrs={"class": "form-control"}),
    )
    created_on_start = DateField(
        required=False,
        label="Startdatum",
        widget=DateInput(format="%d/%m/%Y"),
        input_formats=["%d/%m/%Y"],
    )
    created_on_end = DateField(
        required=False,
        label="Startdatum",
        widget=DateInput(format="%d/%m/%Y"),
        input_formats=["%d/%m/%Y"],
    )
    publication_date_start = DateField(
        required=False,
        label="Publicatiedatum",
        widget=DateInput(format="%d/%m/%Y"),
        input_formats=["%d/%m/%Y"],
    )
    publication_date_end = DateField(
        required=False,
        label="Publicatiedatum",
        widget=DateInput(format="%d/%m/%Y"),
        input_formats=["%d/%m/%Y"],
    )
    expiration_date_start = DateField(
        required=False,
        label="Vervaldatum",
        widget=DateInput(format="%d/%m/%Y"),
        input_formats=["%d/%m/%Y"],
    )
    expiration_date_end = DateField(
        required=False,
        label="Vervaldatum",
        widget=DateInput(format="%d/%m/%Y"),
        input_formats=["%d/%m/%Y"],
    )
    deadline_start = DateField(
        required=False,
        label="Deadline",
        widget=DateInput(format="%d/%m/%Y"),
        input_formats=["%d/%m/%Y"],
    )
    deadline_end = DateField(
        required=False,
        label="Deadline",
        widget=DateInput(format="%d/%m/%Y"),
        input_formats=["%d/%m/%Y"],
    )
    sort = CharField(
        max_length=100,
        required=False,
        label="",
        widget=TextInput(attrs={"class": "form-control"}),
    )
    dir = CharField(
        max_length=100,
        required=False,
        label="",
        widget=TextInput(attrs={"class": "form-control"}),
    )
    show = CharField(
        max_length=100,
        required=False,
        label="",
        widget=TextInput(attrs={"class": "form-control"}),
    )


class SortingForm(Form):
    sort = CharField(
        max_length=100,
        required=False,
        label="",
        widget=TextInput(attrs={"placeholder": "Zoekterm...", "class": "form-control"}),
    )
    dir = ChoiceField(
        choices=((None, "---------"), ("asc", "Ascending"), ("desc", "Descending")),
        required=False,
        label="",
        widget=Select(attrs={"class": "form-control"}),
    )


class CommentFilterForm(Form):
    comment_type = ChoiceField(
        choices=(
            ("internal", "Interne reacties"),
            ("external", "Externe reacties"),
            ("all", "Alle reacties"),
        ),
        required=False,
        label="Filter reacties",
        widget=Select(attrs={"class": "form-control"}),
    )


class SettingForm(ModelForm):
    COLUMN_NAMES = (
        ("id", "ID"),
        ("name", "Naam"),
        ("status", "Status"),
        ("created_on", "Startdatum"),
        ("latest_phase_change", "Looptijd"),
        ("current_phase", "Fase"),
        ("deadline", "Deadline"),
        ("responsible", "Verantwoordelijke"),
        ("tags", "Tags"),
    )
    shown_columns = MultipleArrayField(
        ChoiceField(
            choices=COLUMN_NAMES,
            required=False,
            label="Zichtbare kolommen",
            widget=Select(attrs={"class": "form-control"}),
        ),
        blank=True,
        null=True,
    )


class CaseForm(ModelForm):
    attachments = FileField(
        widget=ClearableFileInput(attrs={"class": "form-control"}),
        required=False,
        label="Voeg bijlage(n) toe (optioneel)",
    )

    def clean_description(self):
        description = self.cleaned_data.get("description", "")
        css_sanitizer = CSSSanitizer(
            allowed_css_properties=settings.BLEACH_ALLOWED_STYLES
        )
        return bleach.clean(
            description,
            tags=settings.BLEACH_ALLOWED_TAGS,
            attributes=settings.BLEACH_ALLOWED_ATTRIBUTES,
            css_sanitizer=css_sanitizer,
        )

    def clean_deadline(self):
        deadline = self.cleaned_data.get("deadline", "")
        if deadline:
            return str(timezone.localtime(deadline))
        return None

    class Meta:
        model = Case
        fields = [
            "casetype",
            "name",
            "description",
            "publication_date",
            "expiration_date",
            "deadline",
            "tags",
            "notes",
        ]
        labels = {
            "casetype": "Type",
            "name": "Naam",
            "description": "Omschrijving",
            "publication_date": "Publicatiedatum",
            "expiration_date": "Vervaldatum",
            "deadline": "Deadline",
            "tags": "Tags",
            "notes": "Notities",
        }

        widgets = {
            "casetype": Select(attrs={"class": "form-control"}),
            "name": TextInput(attrs={"class": "form-control"}),
            "description": Textarea(attrs={"class": "form-control"}),
            "publication_date": DateInput(attrs=dict(type="date")),
            "expiration_date": DateInput(attrs=dict(type="date")),
            "deadline": DateInput(attrs=dict(type="date")),
            "tags": TextInput(attrs={"class": "form-control"}),
            "notes": Textarea(attrs={"class": "form-control"}),
        }


class CommentForm(ModelForm):
    attachments = FileField(
        widget=ClearableFileInput(attrs={"class": "form-control"}),
        required=False,
        label="Voeg bijlage(n) toe (optioneel)",
    )

    def clean_description(self):
        description = self.cleaned_data.get("description", "")
        css_sanitizer = CSSSanitizer(
            allowed_css_properties=settings.BLEACH_ALLOWED_STYLES
        )
        return bleach.clean(
            description,
            tags=settings.BLEACH_ALLOWED_TAGS,
            attributes=settings.BLEACH_ALLOWED_ATTRIBUTES,
            css_sanitizer=css_sanitizer,
        )

    class Meta:
        model = Comment
        fields = ["description"]
        labels = {"description": ""}

        widgets = {"description": Textarea()}


class PhaseForm(Form):
    responsible = ModelChoiceField(
        queryset=User.objects.order_by("name"),
        required=False,
        label="Stel verantwoordelijke in op (optioneel)",
        widget=Select(attrs={"class": "form-control"}),
    )
    phase = ModelChoiceField(
        queryset=CaseTypePhase.objects,
        required=False,
        label="En verander fase naar (optioneel)",
        widget=Select(attrs={"class": "form-control"}),
    )
    tasks = MultipleChoiceField(
        widget=CheckboxSelectMultiple, label="Acties", required=False
    )
    closing_tasks = MultipleChoiceField(
        widget=CheckboxSelectMultiple, label="Afrondende acties", required=False
    )

    def __init__(self, casetype=None, tasks=[], closing_tasks=[], *args, **kwargs):
        super(PhaseForm, self).__init__(*args, **kwargs)

        if casetype:
            self.fields["phase"].queryset = CaseTypePhase.objects.filter(
                casetype=casetype
            )

        if tasks:
            self.fields["tasks"].choices = tasks
        
        if closing_tasks:
            self.fields["closing_tasks"].choices = closing_tasks


class NotificationsForm(Form):
    def __init__(self, *args, user=None, **kwargs):
        super(NotificationsForm, self).__init__(*args, **kwargs)

        all_subscriptions = [
            subscription.phase.id for subscription in user.subscriptions.all()
        ]

        for casetype in CaseType.objects.all():
            self.fields[str(casetype.id)] = ModelMultipleChoiceField(
                queryset=casetype.phases,
                widget=CheckboxSelectMultiple,
                required=False,
                label=casetype.name,
                initial=all_subscriptions,
            )
