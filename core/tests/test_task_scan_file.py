from unittest import mock

import celery.exceptions

from core.utils.clamav import FILE_SCAN, FileScanError
from core.factories import AttachmentFactory
from core.tasks import scan_file
from core.tests import FlowTestCase


class TestTaskScanFileTestCase(FlowTestCase):    
    def setUp(self):
        super().setUp()

        self.file = AttachmentFactory(
            file=self.build_contentfile(
                self.relative_path(__file__, ["assets", "landscape.jpeg"])
            ),
        )
        self.scan = mock.patch("core.utils.clamav.scan").start()
        self.create_incident = mock.patch(
            "core.models.ScanIncidentManager.create_from_attachment"
        ).start()

    def tearDown(self):
        super().tearDown()

    def test_scan_file_without_virus(self):
        scan_file(self.file.file)
        self.file.refresh_from_db()

        self.assertIsNotNone(self.file.last_scan)
        self.assertTrue(self.scan.called)
        self.assertFalse(self.create_incident.called)

    def test_scan_file_with_virus(self):
        self.scan.side_effect = FileScanError(FILE_SCAN.VIRUS, "NL.SARS.PLEIO.665G")

        scan_file(self.file.file)
        self.file.refresh_from_db()

        self.assertTrue(self.file.blocked)
        self.assertTrue(self.create_incident.called)

    def test_scan_file_with_another_error(self):
        self.scan.side_effect = FileScanError(
            status=FILE_SCAN.UNKNOWN, feedback="Not a clue on what happened"
        )

        with self.assertRaises(celery.exceptions.Retry):
            scan_file(self.file.file)

        self.file.refresh_from_db()
        self.assertFalse(self.file.blocked)
        self.assertTrue(self.create_incident.called)