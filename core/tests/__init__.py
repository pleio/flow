import os

from django.test import TestCase
from django.core.files.base import ContentFile


class FlowTestCase(TestCase):
    @staticmethod
    def relative_path(root, path):
        return os.path.join(os.path.dirname(root), *path)

    @staticmethod
    def build_contentfile(path):
        content = open(path, "rb").read()
        return ContentFile(content, os.path.basename(path))