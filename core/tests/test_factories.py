from django.test import TestCase

from core.factories import CaseTypeFactory, CaseTypePhaseFactory
from core.models import CaseType


class CaseFromTestCase(TestCase):

    def test_casetype_factory_works(self):
        name = 'unique'

        CaseTypeFactory(name=name)
        CaseTypeFactory(name=name)

        self.assertTrue(CaseType.objects.filter(name=name).count() == 1)

    def test_phase_factory_works(self):
        # Expected to be ordered relative to one another.
        phase1 = CaseTypePhaseFactory(casetype__name='Something')
        phase2 = CaseTypePhaseFactory(casetype__name='Something')
        phase_n = CaseTypePhaseFactory(casetype__name='Something')

        # Expected to have order start at zero.
        phase = CaseTypePhaseFactory()

        self.assertEqual([0, 1, 2, 0], [
            phase1.order,
            phase2.order,
            phase_n.order,
            phase.order
        ])
