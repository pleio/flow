from unittest import mock

from core.tasks import schedule_scan_attachments
from core.factories import AttachmentFactory
from django.test import override_settings
from core.tests import FlowTestCase

class FileScanTestCase(FlowTestCase):

    def setUp(self):
        super().setUp()

        self.file1 = AttachmentFactory()
        self.file2 = AttachmentFactory()
        self.file3 = AttachmentFactory()

    @mock.patch("core.tasks.FileScanScheduler")
    @override_settings(SCAN_CYCLE_DAYS=1)
    def test_schedule_scan_attachments(self, FileScanScheduler):

        schedule_scan_attachments()
        self.assertTrue(FileScanScheduler.called_with(distance=50))

    @mock.patch("core.tasks.FileScanScheduler.schedule_scan")
    def test_schedule_scan_called(self, schedule_scan):
        schedule_scan_attachments()
        self.assertTrue(schedule_scan.called)
    