from unittest import mock
from core.tests.helpers import FileTestCase
from core.factories import AttachmentFactory
from core.utils import clamav
from core.utils.clamav import FILE_SCAN
from core.shared import scan_file
from core.models import Attachment
from django.utils import timezone


class AttachmentModelTestCase(FileTestCase):
    def setUp(self):
        super().setUp()

        self.file = AttachmentFactory(
            file=self.build_contentfile(
                self.relative_path(__file__, ["assets", "landscape.jpeg"])
            ),
        )
        self.id = self.file.object_id
        self.scan = mock.patch("core.utils.clamav.scan").start()
        self.create_incident = mock.patch(
            "core.models.ScanIncidentManager.create_from_attachment"
        ).start()
    
    
    @mock.patch("core.utils.clamav.scan")
    @mock.patch("core.models.ScanIncidentManager.create_from_attachment")
    def test_scan_file_clean(self, mocked_create_incident, mocked_scan):
        mocked_scan.return_value = FILE_SCAN.CLEAN
        start_date_time = timezone.now()

        result = self.file.scan()
        self.file.refresh_from_db()

        self.assertTrue(result)
        self.assertTrue(self.file.last_scan >= start_date_time)
        self.assertTrue(self.file.last_scan <= timezone.now())
        self.assertTrue(mocked_scan.called)
        self.assertFalse(mocked_create_incident.called)

    @mock.patch("core.utils.clamav.scan")
    @mock.patch("core.models.ScanIncidentManager.create_from_attachment")
    def test_scan_file_with_virus(self, mocked_create_incident, mocked_scan):
        mocked_scan.side_effect = clamav.FileScanError(
            FILE_SCAN.VIRUS, "NL.SARS.PLEIO.Z665"
        )
        start_date_time = timezone.now()

        result = self.file.scan()
        self.file.refresh_from_db()

        self.assertFalse(result)
        self.assertTrue(self.file.last_scan >= start_date_time)
        self.assertTrue(self.file.last_scan <= timezone.now())
        self.assertTrue(mocked_scan.called)
        self.assertTrue(mocked_create_incident.called)

    @mock.patch("core.utils.clamav.scan")
    @mock.patch("core.models.ScanIncidentManager.create_from_attachment")
    def test_file_with_virus_not_deleted(self, mocked_create_incident, mocked_scan):
        mocked_scan.side_effect = clamav.FileScanError(
            FILE_SCAN.VIRUS, "NL.SARS.PLEIO.Z665"
        )
        start_date_time = timezone.now()

        with self.assertRaises(Exception):
            scan_file(self.file)

        result = self.file.scan()
        self.file.refresh_from_db()

        self.assertFalse(result)
        self.assertTrue(self.file.last_scan >= start_date_time)
        self.assertTrue(self.file.last_scan <= timezone.now())
        self.assertTrue(mocked_scan.called)
        self.assertTrue(mocked_create_incident.called)

    @mock.patch("core.utils.clamav.scan")
    @mock.patch("core.models.ScanIncidentManager.create_from_attachment")
    def test_file_with_virus_deleted(self, mocked_create_incident, mocked_scan):
        mocked_scan.side_effect = clamav.FileScanError(
            FILE_SCAN.VIRUS, "NL.SARS.PLEIO.Z665"
        )

        with self.assertRaises(Exception):
            scan_file(self.file, delete_if_virus=True, delete_from_disk=True)

        attachment = Attachment.objects.filter(object_id = self.id)

        self.assertFalse(attachment.exists())
        self.assertTrue(mocked_scan.called)
        self.assertTrue(mocked_create_incident.called)