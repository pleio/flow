from datetime import datetime
from unittest import mock

import pytz
from django.test import TestCase

from core.factories import CaseFactory, CaseTypePhaseFactory
from core.models import Subscription, Case, CsvExport
from core.tasks import (
    calculate_work_hours,
    send_max_duration_case_phase_exceeded_emails,
    sent_max_duration_exceeded_email,
    create_cases_export
)
from pleio_auth.factories import UserFactory


class SentMaxDurationExceededEmailTestCase(TestCase):

    def setUp(self):
        super().setUp()

        self.subscriber1 = UserFactory()
        self.subscriber2 = UserFactory()
        self.non_subscriber = UserFactory()
        self.case_phase = CaseTypePhaseFactory(max_duration_hours=12.0)
        self.case = CaseFactory(current_phase=self.case_phase)
        self.subscription1 = Subscription.objects.create(
            user=self.subscriber1, phase=self.case_phase
        )
        self.subscription2 = Subscription.objects.create(
            user=self.subscriber2, phase=self.case_phase
        )

    @mock.patch("core.tasks.calculate_work_hours")
    @mock.patch("core.tasks.sent_max_duration_exceeded_email.delay")
    def test_send_email_called_workhours(
        self, mocked_sent_max_duration_exceeded_email, total_hours
    ):
        total_hours.return_value = 40
        send_max_duration_case_phase_exceeded_emails.s().apply()
        mocked_sent_max_duration_exceeded_email.assert_called()

    @mock.patch("core.tasks.send_mail")
    @mock.patch("core.tasks.calculate_work_hours")
    @mock.patch("core.tasks.sent_max_duration_exceeded_email")
    def test_send_email_not_called(self, mocked_email, total_hours, mocked_send_mail):
        total_hours.return_value = 8
        send_max_duration_case_phase_exceeded_emails.s().apply()
        mocked_email.assert_not_called()
        mocked_send_mail.assert_not_called()

    @mock.patch("core.tasks.send_mail")
    def test_send_email_called(self, mocked_send_mail):
        sent_max_duration_exceeded_email.s(self.case.id).apply()
        mocked_send_mail.assert_called()


def localize(obj):
    timezone = pytz.timezone("Europe/Amsterdam")
    return timezone.localize(obj)


class CalculateWorkHoursTestCase(TestCase):
    def test_single_full_workday(self):
        start_date = localize(datetime(2023, 10, 2, 9, 00, 00))
        end_date = localize(datetime(2023, 10, 2, 17, 00, 00))
        total_hours = calculate_work_hours(start_date, end_date)
        self.assertEqual(total_hours, 8)

    def test_start_date_after_workday_end(self):
        start_date = localize(datetime(2023, 10, 2, 18, 00, 00))
        end_date = localize(datetime(2023, 10, 3, 17, 00, 00))
        total_hours = calculate_work_hours(start_date, end_date)
        self.assertEqual(total_hours, 8)

    def test_calculate_work_hours_multiple_workdays_around_weekend(self):
        start_date = localize(datetime(2021, 12, 31, 9, 00, 00))
        end_date = localize(datetime(2022, 1, 4, 17, 00, 00))
        total_hours = calculate_work_hours(start_date, end_date)
        self.assertEqual(total_hours, 24)

class ExportTestCase(TestCase):
    def setUp(self):
        super(ExportTestCase, self).setUp()
        self.case1 = CaseFactory()
        self.case2 = CaseFactory()
        self.case3 = CaseFactory()
        self.staff = UserFactory()

    def test_create_cases_export(self):
        case_ids = Case.objects.all()
        create_cases_export(case_ids, self.staff.id)   
        self.assertTrue(CsvExport.objects.filter().exists())