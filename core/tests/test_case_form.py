from django.test import TestCase
from django.utils import timezone

from core.factories import CaseFactory, CaseTypePhaseFactory, SubscriptionFactory
from pleio_auth.factories import UserFactory
from core.models import Case, Comment, Subscription

class CaseFromTestCase(TestCase):
    maxDiff = None

    def setUp(self):
        super(CaseFromTestCase, self).setUp()

        self.staff = UserFactory()
        self.user = UserFactory()
        self.case = CaseFactory(manager=None)
        self.phase1 = CaseTypePhaseFactory(casetype__name=self.case.casetype, order=1)
        self.phase2 = CaseTypePhaseFactory(casetype__name=self.case.casetype, order=2)
        self.case.current_phase = self.phase1
        self.subscription = SubscriptionFactory(user = self.user, phase=self.phase1)

    def case_summary(self, case):
        return [
            case.casetype.id,
            case.name,
            case.description,
            str(case.deadline),
            str(case.tags),
        ]        

    def test_view_cases(self):
        response = self.client.get(f'/')

        self.assertEqual(response.status_code, 302)

        self.client.force_login(self.staff)

        response = self.client.get(f'/', {
            'casetype': self.case.casetype.id,
            'sort': 'id'
        })

        self.assertEqual(response.status_code, 200)

    def test_view_case(self):
        self.client.force_login(self.staff)

        response = self.client.post(f'/cases/view/{self.case.id}/')

        self.assertEqual(response.status_code, 200)

    def test_notification_form(self):
        self.client.force_login(self.staff)

        response = self.client.post(f'/notifications', {
            self.case.casetype.id: self.phase1.id,
        })

        self.staff.save()

        self.assertTrue(Subscription.objects.filter(user = self.staff).exists())
        
        self.assertEqual(response.status_code, 200)

    def test_add_case(self):
        self.client.force_login(self.staff)

        response = self.client.post(f'/cases/add', {
            'casetype': self.case.casetype.id,
            'name': self.case.name,
            'description': self.case.description,
        })

        self.assertEqual(response.status_code, 302)
        new_case_id = self.case.id + 1
        self.assertEqual(response.url, f"/cases/view/{new_case_id}")

    def test_view_case_logs(self):
        self.client.force_login(self.staff)

        response = self.client.post(f'/cases/view/{self.case.id}/logs')

        self.assertEqual(response.status_code, 200)

    def test_claim_case(self):
        self.client.force_login(self.staff)

        response = self.client.post(f'/cases/claim/{self.case.id}', {
            'id': self.case
        })

        self.case.refresh_from_db()
        self.case.save()

        self.assertEqual(response.status_code, 302)
        self.assertEqual(self.case.manager, self.staff)

    def test_unclaim_case(self):
        self.client.force_login(self.staff)

        response = self.client.post(f'/cases/unclaim/{self.case.id}', {
            'id': self.case
        })

        self.case.refresh_from_db()
        self.case.save()

        self.assertEqual(response.status_code, 302)
        self.assertEqual(self.case.manager, None)


    def test_add_edit_delete_comment(self):
        self.client.force_login(self.staff)

        self.case.manager = self.staff

        response = self.client.post(f'/comments/add', {
            'case_id': self.case.id,
            'description': 'test',
            'phase': self.phase1.id,
            'responsible': self.user.id
        })

        self.comment = Comment.objects.filter(case = self.case.id).first()

        self.assertEqual(response.status_code, 302)
        self.assertEqual(response.url, f"/cases/view/{self.case.id}")
        
        response = self.client.post(f'/comments/edit/{self.comment.id}', {
            'description': 'test edit',
            'phase': self.phase2,
        })

        self.assertEqual(response.status_code, 302)
        self.assertEqual(response.url, f"/cases/view/{self.case.id}")  

        response = self.client.post(f'/comments/delete/{self.comment.id}', {
            'case_id': self.case.id
        })

        self.assertEqual(response.status_code, 302)
        self.assertEqual(response.url, f"/cases/view/{self.case.id}")  
        self.assertFalse(Comment.objects.filter(id = self.comment.id).exists())

    def test_update_no_change(self):
        expect = self.case_summary(self.case)

        self.client.force_login(self.staff)
        response = self.client.post(f'/cases/edit/{self.case.id}', {
            'casetype': self.case.casetype.id,
            'name': self.case.name,
            'description': self.case.description,
            'deadline': str(self.case.deadline) if self.case.deadline else "",
            'tags': ', '.join(self.case.tags),
        })
        self.case.refresh_from_db()

        self.assertEqual(response.status_code, 302)
        self.assertEqual(response.url, f"/cases/view/{self.case.id}")
        self.assertEqual(expect, self.case_summary(self.case))

    def test_update_deadline(self):
        FMT = "%Y-%m-%d"
        self.client.force_login(self.staff)

        expected_deadline = timezone.now() + timezone.timedelta(days=200)

        response = self.client.post(f'/cases/edit/{self.case.id}', {
            'casetype': self.case.casetype.id,
            'name': self.case.name,
            'description': self.case.description,
            'deadline': str(expected_deadline),
            'tags': ', '.join(self.case.tags),
        })
        self.case.refresh_from_db()
        actual_deadline = self.case.deadline

        self.assertEqual(actual_deadline.strftime(FMT),
                         expected_deadline.strftime(FMT))

    def test_mark_closed(self):
        self.client.force_login(self.staff)

        response = self.client.post(f'/cases/mark_closed/{self.case.id}',{
            'case_id': self.case.id
        })

        self.assertEqual(response.status_code, 302)
        self.assertEqual(response.url, f"/cases/view/{self.case.id}")

    def test_delete_case(self):
        self.client.force_login(self.staff)

        response = self.client.post(f'/cases/delete/{self.case.id}')

        self.assertEqual(response.status_code, 200)
        self.assertTrue(Case.objects.filter(id = self.case.id).exists())

        response = self.client.post(f'/cases/delete/{self.case.id}', {
            'confirm_id': self.case,
            'id': self.case
        })

        self.assertEqual(response.status_code, 302)
        self.assertFalse(Case.objects.filter(id = self.case.id).exists())
