import csv


class CsvWriter:
    def __init__(self, stream):
        self.writer = csv.writer(stream)

    def write(self, data):
        header = None
        for record in data:
            if not header:
                header = record.keys()
                self.writer.writerow(header)
            self.writer.writerow(record.values())
