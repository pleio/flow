import json
import logging
import os
import shutil

from django.conf import settings
from django.core.management import call_command
from django.utils.timezone import localtime

from backup.io.writer.csv_writer import CsvWriter
from backup.serializer import BackupSerializerBase
from backup.utils import CleanIterator, create_archive
from core.backup_serializers import (CaseBackupSerializer, CaseLogBackupSerializer,
                                     CommentBackupSerializer, ExternalCommentBackupSerializer,
                                     AttachmentBackupSerializer)
from core.models import (Case, CaseLog, Comment, ExternalComment, Attachment)

logger = logging.getLogger(__name__)


class WorkflowExporter():
    def __init__(self, name=None, compress=False, exclude_files=False, casetype=None):
        self.name = name or "%s_%s" % (os.getenv('HOSTNAME'), localtime().strftime("%Y%m%d"))
        self.root = settings.BACKUP_FOLDER
        self.target_path = None
        self.archive_path = None
        self.compress = compress
        self.exclude_files = exclude_files
        self.casetype = casetype
        self.attachments = []

    @property
    def target_name(self):
        return os.path.basename(self.archive_path or self.target_path)

    def _create_folder(self):
        path = os.path.join(self.root, self.name)
        while os.path.exists(path):
            next = CleanIterator(self.name).next()
            path = os.path.join(self.root, f"{self.name}_{next}")
        os.makedirs(path)
        self.target_path = path

    def run(self):
        self._create_folder()
        self._export_commons()
        self._export_cases()
        self._maybe_export_media()
        self._maybe_create_archive()

    def _export_commons(self):
        self._dumpdata('pleio_auth',
                       'core.CaseType', 'core.CaseTypePhase', 'core.Subscription',
                       output=os.path.join(self.target_path, 'auth.json'))

    def _dump_model_to_csv(self, serializer: BackupSerializerBase, queryset, output):
        with open(output, 'w') as stream:
            writer = CsvWriter(stream)
            writer.write(serializer.serialize(queryset))

    def _export_cases(self):
        if self.casetype:
            cases = Case.objects.filter(casetype__name=self.casetype)
        else:
            cases = Case.objects.all()

        self._dump_ids(pks=[str(pk) for pk in cases.values_list('uid', flat=True)],
                       output=os.path.join(self.target_path, 'cases-pks.json'))

        self._dump_model_to_csv(serializer=CaseBackupSerializer(),
                                queryset=cases,
                                output=os.path.join(self.target_path, 'cases.csv'))
        self._collect_attachments(cases)

        self._dump_model_to_csv(serializer=CaseLogBackupSerializer(),
                                queryset=CaseLog.objects.filter(case__in=cases) if self.casetype else CaseLog.objects.all(),
                                output=os.path.join(self.target_path, 'caselogs.csv'))

        comments = Comment.objects.filter(case__in=cases) if self.casetype else Comment.objects.all()
        self._dump_model_to_csv(serializer=CommentBackupSerializer(),
                                queryset=comments,
                                output=os.path.join(self.target_path, 'comments.csv'))
        self._collect_attachments(comments)

        self._dump_model_to_csv(serializer=ExternalCommentBackupSerializer(),
                                queryset=ExternalComment.objects.filter(case__in=cases) if self.casetype else ExternalComment.objects.all(),
                                output=os.path.join(self.target_path, 'externalcomments.csv'))

        self._dump_model_to_csv(serializer=AttachmentBackupSerializer(),
                                queryset=Attachment.objects.all(),
                                output=os.path.join(self.target_path, 'attachments.csv'))

    def _collect_attachments(self, queryset):
        for item in queryset.all():
            self.attachments.extend([a.id for a in item.attachments.all()])

    def _dumpdata(self, *args, **kwargs):
        call_command("dumpdata",
                     *args,
                     **kwargs,
                     natural_foreign=True,
                     natural_primary=True,
                     format='json',
                     indent=4)

    def _dump_ids(self, pks, output):
        with open(output, 'w') as fh:
            json.dump(pks, fh, indent=4)

    def _maybe_export_media(self):
        if self.exclude_files:
            return
        root = os.path.join(self.target_path, 'media')
        for attachment in Attachment.objects.filter(id__in=self.attachments):
            destination = os.path.join(root, attachment.file.name)
            if not os.path.exists(os.path.dirname(destination)):
                os.makedirs(os.path.dirname(destination))
            if os.path.exists(attachment.file.path):
                shutil.copy2(attachment.file.path, destination)

    def _maybe_create_archive(self):
        if not self.compress:
            return

        archive_path = os.path.join(self.root, f"{self.name}.zip")
        while os.path.exists(archive_path):
            next = CleanIterator(self.name + '.zip').next()
            archive_path = os.path.join(self.root, f"{self.name}_{next}.zip")

        self.archive_path = create_archive(self.target_path, archive_path)
        shutil.rmtree(self.target_path)
