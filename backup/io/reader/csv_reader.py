import csv


class CsvReader:
    def __init__(self, stream):
        self.reader = csv.reader(stream)

    def read(self):
        header = None
        for record in self.reader:
            if not header:
                header = record
                continue
            yield {k: v for k, v in zip(header, record)}
