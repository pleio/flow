import json
import os
import shutil

from django.conf import settings
from django.core.management import call_command

from backup.io.reader.csv_reader import CsvReader
from backup.serializer import BackupSerializerBase
from backup.utils import CleanIterator, extract_archive
from core.backup_serializers import (CaseBackupSerializer, CommentBackupSerializer, CaseLogBackupSerializer,
                                     ExternalCommentBackupSerializer, AttachmentBackupSerializer)
from core.models import Attachment


class WorkflowImporter():
    def __init__(self, name, root=None):
        self.name = name
        self.root = root or settings.BACKUP_FOLDER
        self.path = None
        self.copied_files = 0
        self._clean_me_up = []
        self.summary = {}

    def run(self):
        self._extract_input()
        self._import_commons()
        self._import_cases()
        self._import_media()
        self._cleanup()

    def _cleanup(self):
        for item in self._clean_me_up:
            if os.path.isfile(item):
                os.unlink(item)
            if os.path.isdir(item):
                shutil.rmtree(item)

    def _extract_input(self):
        maybe_folder_source = os.path.join(self.root, self.name)

        assert os.path.exists(maybe_folder_source), "Source backup file/folder does not exist."

        if maybe_folder_source.endswith('.zip'):
            assert os.path.isfile(maybe_folder_source), "Source backup file appears to be a folder"

            folder_source = self._foldername_from_filename(maybe_folder_source)
            while os.path.exists(folder_source):
                next = CleanIterator(self.name).next()
                folder_source = f"{self._foldername_from_filename(maybe_folder_source)}_{next}"

            extract_archive(maybe_folder_source, folder_source)
            self.path = folder_source
            self._clean_me_up.append(self.path)
        else:
            assert os.path.isdir(maybe_folder_source), "Source backup folder appears to be a file"
            self.path = maybe_folder_source

    def _foldername_from_filename(self, filename):
        folder_name, ext = os.path.splitext(filename)
        return folder_name

    def _import_commons(self):
        call_command("loaddata", os.path.join(self.path, 'auth.json'))

    def _import_cases(self):
        self._load_csv(CaseBackupSerializer(), os.path.join(self.path, 'cases.csv'))
        self._load_csv(CaseLogBackupSerializer(), os.path.join(self.path, 'caselogs.csv'))
        self._load_csv(CommentBackupSerializer(), os.path.join(self.path, 'comments.csv'))
        self._load_csv(ExternalCommentBackupSerializer(), os.path.join(self.path, 'externalcomments.csv'))
        self._load_csv(AttachmentBackupSerializer(), os.path.join(self.path, 'attachments.csv'))

    def _load_csv(self, serializer: BackupSerializerBase, path):
        objects = 0
        with open(path, 'r') as stream:
            reader = CsvReader(stream)
            for record in reader.read():
                serializer.materialize_item(record)
                objects += 1
        self.summary[os.path.basename(path)] = objects

    def _import_media(self):
        for attachment in Attachment.objects.all():
            if not os.path.exists(attachment.file.path):
                expected_copy = os.path.join(self.path, 'media', attachment.file.name)
                if not os.path.exists(expected_copy):
                    continue
                if not os.path.exists(os.path.dirname(attachment.file.path)):
                    os.makedirs(os.path.dirname(attachment.file.path))
                shutil.copy2(expected_copy, attachment.file.path)
                self.copied_files += 1
