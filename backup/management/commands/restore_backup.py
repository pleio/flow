from django.core.management import BaseCommand

from backup.io.materialize import WorkflowImporter


class Command(BaseCommand):
    def add_arguments(self, parser):
        super().add_arguments(parser)
        parser.add_argument('--name', help="Backup file or folder name", default=None)
        parser.add_argument('--root', help="Path to the folder to search for the source.", default=None)

    def handle(self, *args, **options):
        assert options['name'], "'--name' is manditory."

        importer = WorkflowImporter(name=options['name'], root=options['root'])
        importer.run()

        for file, count in importer.summary.items():
            self.stdout.write("Installed %s object(s) from %s." % (count, file))
        self.stdout.write("Copied %s media file(s)." % importer.copied_files)
        self.stdout.write("Import complete!\n")
