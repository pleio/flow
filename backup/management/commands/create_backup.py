from django.core.management import BaseCommand

from backup.io.serialize import WorkflowExporter


class Command(BaseCommand):
    def add_arguments(self, parser):
        super().add_arguments(parser)
        parser.add_argument('--name', help="The backup name", default=None)
        parser.add_argument('--exclude-files', help="Do not export user-uploaded media", action="store_true", default=False)
        parser.add_argument('--compress', help="Create a zip-archive", action="store_true", default=False)
        parser.add_argument('--casetype', help="Only export cases of this casetype", default=None)

    def handle(self, *args, **options):
        exporter = WorkflowExporter(name=options['name'],
                                    compress=options['compress'],
                                    exclude_files=options['exclude_files'],
                                    casetype=options['casetype'])
        exporter.run()
        print(exporter.target_name)