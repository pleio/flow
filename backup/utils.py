import errno
import os
import zipfile
from collections import defaultdict


class CleanIterator():
    db = defaultdict(lambda: 0)

    @classmethod
    def _next(cls, key):
        cls.db[key] += 1
        return cls.db[key]

    def __init__(self, key):
        self.key = key

    def next(self):
        return self._next(self.key)


def create_archive(source_folder, target_file):
    zip_file = zipfile.ZipFile(target_file, mode='w', compression=zipfile.ZIP_DEFLATED)
    _add_to_zip_recursive(source_folder, '', zip_file)
    zip_file.close()
    return target_file


def _add_to_zip_recursive(root, path, zip_file):
    try:
        for filename in os.listdir(os.path.join(root, path)):
            _add_to_zip_recursive(root=root,
                                  path=os.path.join(path, filename),
                                  zip_file=zip_file)
    except NotADirectoryError:
        if path == '':
            zip_file.write(root, arcname=os.path.basename(root))
        else:
            zip_file.write(os.path.join(root, path), arcname=path)


def extract_archive(archive_path, target_path):
    with zipfile.ZipFile(archive_path, "r") as zip_ref:
        if os.path.exists(target_path):
            raise IsADirectoryError(errno.EISDIR,
                                    os.strerror(errno.EISDIR), target_path)
        zip_ref.extractall(target_path)
    return target_path
