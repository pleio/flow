# Usage

Get the values file from 1password

Deploy new:

```
helm install -n flow -f helm/flow/values.flow-test.yaml flow-test helm/flow --dry-run
```

Update deployment:

```
helm upgrade -n flow -f helm/flow/values.flow-test.yaml flow-test helm/flow --dry-run
```

Where `flow-test` is the deployment name.