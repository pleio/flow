from celery.schedules import crontab

beat_schedule = {
    "hourly": {
        "task": "core.tasks.dispatch_hourly_cron",
        "schedule": crontab(minute="45", hour="*"),
    },
    "file_scan": {
        "task": "file.tasks.schedule_scan_attachments",
        "schedule": crontab(minute=15, hour=23),
    }
}
