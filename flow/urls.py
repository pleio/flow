"""flow URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.10/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  re_path(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  re_path(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  re_path(r'^blog/', include('blog.urls'))
"""

from django.conf import settings
from django.conf.urls import include
from django.conf.urls.static import static
from django.contrib import admin
from django.urls import re_path
from django.views.generic import RedirectView
from rest_framework import routers

import pleio_auth.views as auth_views
from core import api, views

router = routers.DefaultRouter()
router.register(r"cases", api.CaseViewSet)
router.register(r"casetypes", api.CaseTypeViewSet)
router.register(r"caselogs", api.CaseLogViewSet)
router.register(r"externalcomments", api.ExternalCommentViewSet)

urlpatterns = [
    re_path(r"^$", views.cases, name="cases"),
    re_path(r"^media/(?P<path>.*)$", views.protected_media, name="protected_media"),
    re_path(r"^notifications", views.notifications, name="notifications"),
    re_path(r"^cases/add", views.add_case, name="add_case"),
    re_path(r"^cases/view/(?P<id>[0-9]+)/$", views.view_case, name="view_case"),
    re_path(
        r"^cases/view/(?P<id>[0-9]+)/logs$", views.view_case_logs, name="view_case_logs"
    ),
    re_path(r"^cases/claim/(?P<id>[0-9]+)$", views.claim_case, name="claim_case"),
    re_path(r"^cases/unclaim/(?P<id>[0-9]+)$", views.unclaim_case, name="unclaim_case"),
    re_path(
        r"^cases/mark_closed/(?P<id>[0-9]+)$", views.mark_closed, name="mark_closed"
    ),
    re_path(
        r"^cases/remove_responsible/(?P<id>[0-9]+)$",
        views.remove_responsible,
        name="remove_responsible",
    ),
    re_path(r"^cases/edit/(?P<id>[0-9]+)", views.edit_case, name="edit_case"),
    re_path(r"^cases/delete/(?P<id>[0-9]+)", views.delete_case, name="delete_case"),
    re_path(r"^comments/add", views.add_comment, name="add_comment"),
    re_path(r"^comments/edit/(?P<id>[0-9]+)", views.edit_comment, name="edit_comment"),
    re_path(
        r"^comments/delete/(?P<id>[0-9]+)", views.delete_comment, name="delete_comment"
    ),
    re_path(r"^auth/", include("pleio_auth.urls")),
    re_path(r"^admin/login/", auth_views.login, name="admin_login"),
    re_path(r"^admin/", admin.site.urls),
    re_path(r"^api/", include(router.urls)),
]

if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
